<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>
    <?php
if (isset($pageTitle)) {
  echo $pageTitle;
} else {
  echo " Title Text";
}
?>
  </title>
  <meta name="description" content="<?php
if (isset($pageDescription)) {
  echo $pageDescription;
} else {
  echo " Description Text ";
}
?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="HandheldFriendly" content="True">
  <meta name="author" content="Driven Local">
  <link rel="shortcut icon" href="img/favicon.png">
  <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
  <!-- NO SEO - REMOVE BEFORE GOING LIVE -->
  <!-- Site CSS -->
  <link type='text/css' rel="stylesheet" href="css/main.css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Playfair+Display|Raleway" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Italianno|Libre+Baskerville|Martel:200|Old+Standard+TT:400i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Droid+Sans+Mono|Playfair+Display+SC" rel="stylesheet">
  <script src="js/vendor/modernizr.js"></script>
  <!-- Google Analytics Tag -->
</head>

<body>
  <header class="headbar " role="navigation">
    <div class="logo">
      <a href="./" title="Bootstrap Starter Files" rel="home" class="">
        <img src="images/logo2.png">
      </a>
    </div>
    <a href="tel:8777404024" class="call">Call <span>877-740-4024</span></a>
    <div class="social">
      <a target="_blank" href="https://www.facebook.com/ElementsDistinctiveLighting/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
      <a target="_blank" href="https://twitter.com/elementslights"><i class="fa fa-twitter" aria-hidden="true"></i></a>
      <a target="_blank" href="http://www.houzz.com/pro/distinctiveelements/distinctive-elements-kitchen-and-home-design"><i class="fa fa-houzz" aria-hidden="true"></i></a>
      <a target="_blank" href="https://plus.google.com/+ElementsDistinctiveLightingCarlePlace"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
      <a target="_blank" href=""><i class="fa fa-pinterest-p" aria-hidden="true"></i></a>
      <a target="_blank" href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
    </div>
    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
      <span class="sr-only">Toggle navigation</span>
      <i class="fa fa-bars"></i>
    </button>
    <nav class="collapse navbar-collapse" id="navbar-main">
      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="./" <?php if ($thisPage ==('Home')) echo 'class="current"' ?>>Home</a>
        </li>
        <li>
          <a href="portfolio" <?php if ($thisPage ==('Portfolio')) echo 'class="current"' ?>>Portfolio</a>
        </li>
        <!--  <li class="dropdown show-on-hover">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" href="#">Portfolio</a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Sublink</a></li>
                        <li><a href="#">Sublink</a></li>
                        <li><a href="#">Sublink</a></li>
                    </ul>
                </li> -->
        <li>
          <a href="style-quiz" <?php if ($thisPage ==('Style Quiz')) echo 'class="current"' ?>>Style Quiz</a>
        </li>
        <li>
          <a href="recognition" <?php if ($thisPage ==('Recognition')) echo 'class="current"' ?>>Recognition</a>
        </li>
        <li>
          <a href="trade-professional" <?php if ($thisPage ==('Trade Professional')) echo 'class="current"' ?>>Trade Professional</a>
        </li>
        <li>
          <a href="proportions" <?php if ($thisPage ==('Proportion Guide')) echo 'class="current"' ?>>Proportion Guide</a>
        </li>
        <li>
          <a class="blogtitle" href="https://elementsdistinctivelighting.wordpress.com/" target="_blank">Blog</a>
        </li>
      </ul>
    </nav>
    <div class="container-fluid"></div>
  </header>
