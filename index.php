<?php
$pageTitle = "Elements Distictive Lighting & Home Furnishings";
$pageDescription = "Description";
$thisPage = "Home";
$servicePage = false;
include('inc/header.php'); ?>
  <section class="flexslider">
    <ul class="slides">
      <li>
        <img src="images/new-slide1.jpg" />
        <div class="flex-caption">
          <!-- <div class="flexinner">
                <h3>What's your style?  </h3>
                <p>Don't Know, We have a little quiz to help you.</p>
                <div class=""><a href="style-quiz"><i class="fa fa-caret-right" aria-hidden="true"></i> Take Our Quiz</a></div>
            </div> -->
          <div class="flexinner">
            <h3>Featured Products  </h3>
            <p>Large Drum Style Chandelier</p>
            <div class=""><a href="portfolio"><i class="fa fa-caret-right" aria-hidden="true"></i> View Portfolio</a></div>
          </div>
        </div>
      </li>
      <li>
        <img src="images/new-slide2.jpg" />
        <div class="flex-caption">
          <div class="flexinner">
            <h3>Featured Products  </h3>
            <p>Large Drum Style Chandelier</p>
            <div class=""><a href="portfolio"><i class="fa fa-caret-right" aria-hidden="true"></i> View Portfolio</a></div>
          </div>
        </div>
      </li>
      <li>
        <img src="images/new-slide3.jpg" />
        <div class="flex-caption">
          <div class="flexinner">
            <h3>Featured Products  </h3>
            <p>Large Drum Style Chandelier</p>
            <div class=""><a href="portfolio"><i class="fa fa-caret-right" aria-hidden="true"></i> View Portfolio</a></div>
          </div>
        </div>
      </li>
    </ul>
  </section>
  <div class="fadeline"></div>
  <section class="homecontent">
    <div class="row">
      <div class="col-md-8  col-sm-12 col-sm-pull-0">
        <div class="row">
          <div class="col-md-12 col-xs-12">
            <div class="grid">
              <figure class="effect-layla">
                <img src="images/home1.jpg" width="100%">
                <figcaption>
                  <h2>View Our <span>Portfolio</span></h2>
                  <p>With over 18,000 square feet of showroom space, you're sure to find what you're looking for.... and then some</p>
                  <a href="portfolio">View more</a>
                </figcaption>
              </figure>
            </div>
          </div>
          <div class="col-md-12 col-xs-12">
            <div class="grid">
              <figure class="effect-layla">
                <img src="images/home5.jpg" width="100%">
                <figcaption>
                  <h2>Take Our <span>Style Quiz</span></h2>
                  <p>Our buyers travel the globe to secure the latest cutting edge designs that you won't find anywhere else.</p>
                  <a href="style-quiz">View more</a>
                </figcaption>
              </figure>
            </div>
          </div>
          <div class="col-md-12 col-xs-6">
            <img src="images/home2.jpg" width="100%">
          </div>
          <div class="col-md-12 col-xs-6">
            <img src="images/home4.jpg" width="100%">
          </div>
          <!-- <div class="col-md-12 col-xs-6"><img src="images/home4.jpg" width="100%"></div> 
       <div class="col-md-12 col-xs-6"><img src="images/home5.jpg" width="100%"></div> 
        <div class="col-md-12 col-xs-6"><img src="images/home2.jpg" width="100%"></div>
        <div class="col-md-12 col-xs-6"><img src="images/home1.jpg" width="100%"></div>-->
        </div>
      </div>
      <div class="col-md-4 col-sm-12 col-sm-push-0">
        <div class="row">
          <div class="col-md-12 col-sm-6">
            <div class="content">
              <h1>Welcome to Elements</h1>
              <h2>Distinctive Lighting &amp; Home Furnishings</h2>
              <blockquote> "For more than a quarter-century, we have been proud to provide discriminating customers with unsurpassed expertise and unparalleled products."</blockquote>
              <div class="largertext">Whether looking for lighting, accent furniture, unique home accents, or wall decor, you can always count on us to listen and provide thoughtful, creative solutions. </div>
              <div><a href="recognition" class="mono"><i class="fa fa-caret-right" aria-hidden="true"></i> Read More</a></div>
            </div>
          </div>
          <div class="col-md-12 col-sm-6">
            <img src="images/home3.jpg" width="100%">
          </div>
        </div>
      </div>
  </section>
  <?php include ('inc/footer.php'); ?>
