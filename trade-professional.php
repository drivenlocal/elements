<?php
$pageTitle = "Trade Professional | Elements Distictive Lighting & Home Furnishings";
$pageDescription = "Description";
$thisPage = "Trade Professional";
$servicePage = false;
include('inc/header.php'); ?>
  <section class="insidecontent quizinner">
    <div class="row">
      <div class="col-md-12">
        <div class="stylequiz-professional">
          <h1>Trade Professional</h1>
        </div>
        <div class="container-fluid">
          <div class="content">
            <p>As a member of the design trade, you may be eligible to enroll in our Elements Trade Program. Please click on the link below for further details.</p>
            <a target="_blank" href="http://elementstrade.com/">Elements Trade Program</a>
          </div>
        </div>
        <!-- <div class="styles ">
  <div class="row ">
    <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Hollywood Glam </div>
    <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Timeless Elegance </div>
    <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Cutting Edge </div>
    <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Modern Classic </div>
    <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Sophisticated </div>
    <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Farmhouse Chic </div>
  </div>
</div>
 -->
      </div>
  </section>
  <?php include ('inc/footer.php'); ?>
