<?php
$pageTitle = "Our Portfolio | Elements Distictive Lighting & Home Furnishings";
$pageDescription = "Description";
$thisPage = "Portfolio";
$servicePage = false;
include('inc/header.php'); ?>
<section class="insidecontent quizinner"> 
    <div class="row">
        <div class="col-md-12">
            <div class="portfolio">
                <h1>Our Portfolio</h1>
            </div>
            <div class="container-fluid">
                <div class="content">
                    <h2>Click each image below for more details!</h2>
                    <p>
                        With over 18,000 square feet of showroom space, Elements Distinctive Lighting and Home Furnishings has an astounding selection of lighting, mirrors, occasional furniture, accessories and home accents. Here's a sneak peak...
                    </p><br>
                    <div class="row gallery">
                        
                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/505181.jpg">
                                <img src="images/portfolio/505181.jpg" alt="Rustic-chic design captures a historical feel with its solid distressed wood reminiscent of weathered ash, steel construction and iron rust finish. A pear-shaped wood finial adds an additional elegant detail." class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=505181&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/5-1745BK.jpg">
                                <img src="images/portfolio/5-1745BK.jpg" alt="Clean transitional style features a minimalist body that frames the contemporary, candle-like etched opal glass. A large cast ring finial perches on top for a traditional finishing touch to this sleek lantern design." class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=439701&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/5-1424AI.jpg">
                                <img src="images/portfolio/5-1424AI.jpg" alt="Cast metal details are enhanced by its hand-applied Aged Iron finish with copper highlights. Stunning amber etched inner glass is surrounded by panels of clear seedy outer glass, creating a grand statement." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=268562&page=1">BUY NOW</a>
                        </div>
                                
                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/2-5628-CH.jpg">
                                <img src="images/portfolio/2-5628-CH.jpg" alt="Crystal type - Clear Smooth Oysters Polished Chrome Chandelier Draped with Oyster Crystal Accented with a Silk Shade" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=255354&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/2-537-WW.jpg">
                                <img src="images/portfolio/2-537-WW.jpg" alt="White Wrought Iron Chandelier">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=330461&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/6-f2936_5wow_af.jpg">
                                <img src="images/portfolio/6-f2936_5wow_af.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=553613&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/5-29703VI.jpg">
                                <img src="images/portfolio/5-29703VI.jpg" alt="Hand-crafted interior/ exterior feature bold overlapping metal ribbons in a Vintage Iron finish. Paired with seedy glass, this transitional design will illuminate any outdoor oasis." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=749676&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/67-215-45.png">
                                <img src="images/portfolio/67-215-45.png" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=710834&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/2-2259-PN.jpg">
                                <img src="images/portfolio/2-2259-PN.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=659946&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/5-3357-DZ.jpg">
                                <img src="images/portfolio/5-3357-DZ.jpg" alt="Minimalist beauty emphasizes 'less is more' with vintage industrial style. This clean, airy tapered cage design is constructed without glass and the unique square candle sleeves rest on a discreet _x0013_H_x0014_-shaped cluster." class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=711518&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/78-SHK301N-SS1S.png">
                                <img src="images/portfolio/78-SHK301N-SS1S.png" alt="" class="img-responsive">
                            </a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/20-7863-48A.png">
                                <img src="images/portfolio/20-7863-48A.png" alt="" class="img-responsive">
                            </a>
                        </div>
                        
                        <div class="clearfix"></div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/11-43581AVI.jpg">
                                <img src="images/portfolio/11-43581AVI.jpg" alt="The dressed up rustic styling of this 3 light convertible chandelier / semi flush ceiling light. Open Iron Anvil mesh surrounds and protects the Vetro Mica shades" class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=615303&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/11-43703-NBR.jpg">
                                <img src="images/portfolio/11-43703-NBR.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_result.asp?params=ENumber:870837;">BUY NOW</a>
                        </div>
                                
                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/25-59195.jpg">
                                <img src="images/portfolio/25-59195.jpg" alt="" class="img-responsive">
                            </a>
                        </div>

                        <div class="clearfix"></div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/5-5234CM.jpg">
                                <img src="images/portfolio/5-5234CM.jpg" alt="A retro-inspired design updated for todays contemporary decors. The heavy rectangular tube construction has forged details and a unique fitted glass panel design." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=398344&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/16-5916-PN.png">
                                <img src="images/portfolio/16-5916-PN.png" alt="Mixing the timely with the timeless, American designers created an ultra glamorous look that defines the silver screen's golden age. A sleek and elegantly proportioned chandelier in a cut and polished cube of modern acrylic glass" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=568482&page=1">BUY NOW</a>
                        </div>
                                
                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/67-220-48.png">
                                <img src="images/portfolio/67-220-48.png" alt="" class="img-responsive">
                            </a>
                             <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=710858&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/2-2265-AG.jpg">
                                <img src="images/portfolio/2-2265-AG.jpg" alt="A timeless pattern that gives a grpahic feel without overpowering a space. The white linen shade adds texture while a glass diffuser softens the light." class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=616231&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/2-1585-CH.jpg">
                                <img src="images/portfolio/2-1585-CH.jpg" alt="Crystal type - Glass Balls Polished Chrome Chandelier" class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=362176&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/2-584-MT-GA.jpg">
                                <img src="images/portfolio/2-584-MT-GA.jpg" alt="Fixture is like a piece of modern art with bold shapes and lines. The English Bronze and Antique Gold finish have wonderful textures and movement." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=692777&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/2-506-EB-GA.jpg">
                                <img src="images/portfolio/2-506-EB-GA.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=506838&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/6-F2739.jpg">
                                <img src="images/portfolio/6-F2739.jpg" alt="Loosely inspired by a bird cage. Laser cut strapping arranged in a circular design creates the 'cage,' and when lit creates a magnificent pattern on the ceiling. The soft curves of the silhouette and Brushed Steel candle covers are in dramatic contrast to the dark, rich Textured Black finish" class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=415529&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/6-P1234RI.jpg">
                                <img src="images/portfolio/6-P1234RI.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=415633&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/5-4776SL.jpg">
                                <img src="images/portfolio/5-4776SL.jpg" alt="Elegant petite candelabra iron design, finished in authentic Silver Leaf, makes a sophisticated statement with luxurious amber pearl crystals, optional translucent organza shade and ornate details." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=359118&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/45-E20113-24.jpg">
                                <img src="images/portfolio/45-E20113-24.jpg" alt="Each unique glass cover supports handmade glass starbursts - available in clear, blue, green, amber or violet - suspending each burst of color in its own glass globe. Chrome hardware and a satin nickel-finish canopy complete each structure, and every little piece is adjustable to enhance your galaxy in a variety of different ways." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=203924&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/45-E20263-10.jpg">
                                <img src="images/portfolio/45-E20263-10.jpg" alt="Subtle and sleek, Hagen suspends thin disks of beveled glass from barely visible strands of filament. Each disk holds a cool LED light that plays across the surface of the glass and culminates in a crisp ring of beveled luminescence. In single and four-light pendants, Hagen slips unobtrusively into contemporary, sophisticated settings." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=289830&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/45-E20518-18PC.jpg">
                                <img src="images/portfolio/45-E20518-18PC.jpg" alt="The new Larmes features an extended profile Clear glass shade that screws on to a Satin Nickel mount. Long life G4 LED give considerable light and are dimmable." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=532141&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/45-E21201-10PC.jpg">
                                <img src="images/portfolio/45-E21201-10PC.jpg" alt="Mirrored Glass Envelops A Sphere Of Xenon And Seems To Emanate The Heart And Soul Of The Space It Inhabits. Biology meets technology in the Sens life source. Mirrored glass envelops a sphere of Xenon and seems to emanate the heart and soul of the space it inhabits. The clean, simple orb falls gracefully from the ceiling on its gently swirling lifeline." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=306379&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/45-E21300-10PC.jpg">
                                <img src="images/portfolio/45-E21300-10PC.jpg" alt="Legendary Incan tears of the sun encrust the clearest crystal cylinder producing a radiant glimmer of ancient riches. A gold glow of Xenon sparkles through a laser-cut metal sheath with precision, perfectly illuminating the treasures that adorn its outer transparent layer." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=283646&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/45-E21302-10PC.jpg">
                                <img src="images/portfolio/45-E21302-10PC.jpg" alt="Legendary Incan Tears Of The Sun Encrust The Clearest Crystal Cylinder Producing A Radiant Glimmer Of Ancient Riches. Legendary Incan tears of the sun encrust the clearest crystal cylinder producing a radiant glimmer of ancient riches. A gold glow of Xenon sparkles through a laser-cut metal sheath with precision, perfectly illuminating the treasures that adorn its outer transparent layer." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=332863&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/45-E21308-10BZ.jpg">
                                <img src="images/portfolio/45-E21308-10BZ.jpg" alt="Legendary Incan Tears Of The Sun Encrust The Clearest Crystal Cylinder Producing A Radiant Glimmer Of Ancient Riches. Legendary Incan tears of the sun encrust the clearest crystal cylinder producing a radiant glimmer of ancient riches. A gold glow of Xenon sparkles through a laser-cut metal sheath with precision, perfectly illuminating the treasures that adorn its outer transparent layer." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=532150&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/45-E22092-28.jpg">
                                <img src="images/portfolio/45-E22092-28.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=181583&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/19-2855OI.jpg">
                                <img src="images/portfolio/19-2855OI.jpg" alt="One of our most popular collections, elegante combines lighting with decorating. Delicate leaves and crystals top the wrap-around vines. Offered in etruscan gold or oil rubbed bronze." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=260522&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/19-21149SCRE.jpg">
                                <img src="images/portfolio/19-21149SCRE.jpg" alt="Simple straight line frame fashioned in minimalist style finished in our rustic ebony are electrified by the warmth and beauty of the stone candle shades. These shades are carved out of solid stone each has a personality of its own." class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=256081&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/19-21460NKPN.jpg">
                                <img src="images/portfolio/19-21460NKPN.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=423164">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/19-21501DWOI.jpg">
                                <img src="images/portfolio/19-21501DWOI.jpg" alt="The classic finesse collection features sweeping oval tube arms that transition to sculptured fonts. The pleated glass shade in dusty white or frost shed a warm diffused light. The body is finished in satin nickel or oil rubbed bronze." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=713967&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/20-EC1236N-401S4.png">
                                <img src="images/portfolio/20-EC1236N-401S4.png" alt="" class="img-responsive">
                            </a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/20-AM5410-22S.jpg">
                                <img src="images/portfolio/20-AM5410-22S.jpg" alt="" class="img-responsive">
                            </a>
                        </div>

                        <div class="clearfix"></div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/19-39882BCBZ.jpg">
                                <img src="images/portfolio/19-39882BCBZ.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=423233&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/16-989-PN-WS.png">
                                <img src="images/portfolio/16-989-PN-WS.png" alt="Dayton's strong arms hold smooth crystal columns, for a look of confident glamour. The chandelier's central crystal teardrop showcases the material's pristine beauty. Softly textured tailored shades balance the sheen of Dayton's glass and metal." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=528074&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/16-9317-AGB.png">
                                <img src="images/portfolio/16-9317-AGB.png" alt="A brilliant mix of materials, Wellington's sleek and polished planes evoke the glamour of high modernism. Cast metals mingle with gleaming acrylics in a sophisticated play of the angular and ephemeral. Our devotion to design unity shows in the detailed candle sleeves, which top the mixed material motif with vintage tungsten filament lamps." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=644026&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/2-2254-PN.jpg">
                                <img src="images/portfolio/2-2254-PN.jpg" alt="Libby Langdon: 'I truly believe design is in the details. An element I love with the Westwood collection is how the top loop mimics the same angles as the lower metal fretwork. It's a small attention to deatil but a big part of the overall look because it artfully repeats the design theme and helps to balance the fixture'" class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=659941&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/2-2245-DB.jpg">
                                <img src="images/portfolio/2-2245-DB.jpg" alt="The Sylvan Collection was created for homeowners who love lighting that is grand in scope but not clunky and oversized. The sleek lines and angular details of the outer frame is softened by the texture of the shades and the curved lines of the arms. This collection featurs graceful, sloping, polished nickel arms with white linen shades." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=616225&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/2-136-CH.jpg">
                                <img src="images/portfolio/2-136-CH.jpg" alt="Chandelier with Clear smooth glass balls accents with Polished Chrome finish on a solid brass frame." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=426970&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/6-P1312SLV.jpg">
                                <img src="images/portfolio/6-P1312SLV.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=553821&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/6-P1236ASTB.jpg">
                                <img src="images/portfolio/6-P1236ASTB.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=415635&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/6-F27406BUS.jpg">
                                <img src="images/portfolio/6-F27406BUS.jpg" alt="The unique, disk-shaped Sunflower Shape Bauhinia Crystals are suspended within intricate metal work featuring a Burnished Silver finish, which combine to create these contemporary and sparkling silhouettes, finished with a faceted crystal ball accent. The open, airy feeling of the metal work allows for installation in smaller spaces without overpowering the room" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=415530&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/6-F25374slp.jpg">
                                <img src="images/portfolio/6-F25374slp.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=302655&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/5-3521BC.jpg">
                                <img src="images/portfolio/5-3521BC.jpg" alt="Charlotte is an updated traditional design featuring a richly textured off-white fabric shade captured by wide solid metal uprights and trim rings. The domed bottom diffuser is captured inside a heavy trim ring and held by cast decorative knobs for a refined vintage touch." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=554416&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/5-3000PL.jpg">
                                <img src="images/portfolio/5-3000PL.jpg" alt="Wingate offers tradition with a twist: clear, floating beveled glass panels surround understated cast round candle cups and candles sleeves, giving it a modern yet classic style." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=398310&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/20-1243-22.png">
                                <img src="images/portfolio/20-1243-22.png" alt="" class="img-responsive">
                            </a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/11-42046OZ.jpg">
                                <img src="images/portfolio/11-42046OZ.jpg" alt="This generous, bowed clear glass fixture features an Olde Bronze finish and a distinctive Vintage Squirrel Cage Filament bulb that leaves an impact" class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=438168&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/11-43118NBR.jpg">
                                <img src="images/portfolio/11-43118NBR.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=702178&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/11-43565SGD.jpg">
                                <img src="images/portfolio/11-43565SGD.jpg" alt="Featuring stunning cut and polished faceted crystals, this 4 light chandelier from the Casilda Collection is a decorating coup for the updated traditional lifestyle that inspired it. The crystals dazzle alongside the Sterling Gold finish while the Silver Beige fabric shades and satin etched diffusers complete the look." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=615296&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/60-9356.jpg">
                                <img src="images/portfolio/60-9356.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=517998&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/60-9494.jpg">
                                <img src="images/portfolio/60-9494.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=564163&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/2-8104-PN.jpg">
                                <img src="images/portfolio/2-8104-PN.jpg" alt="Chrome, glass and crystal is a combination that's both retro and modern. But it's always high style, as the Paxton collection proves. There's not a curve in sight on these polished nickel frames, which are decorated with just a hint of crystal and glass and finished with lovely white shades." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=557964&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/2-590-EB-GA.jpg">
                                <img src="images/portfolio/2-590-EB-GA.jpg" alt="Luna is lighting that makes a statement. Each fixture is like a piece of modern art with bold shapes and lines. The English Bronze and Antique Gold finish have wonderful textures and movement." class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=692785&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/6-F28108PN.jpg">
                                <img src="images/portfolio/6-F28108PN.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=499400&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/5-FR43538PNI.jpg">
                                <img src="images/portfolio/5-FR43538PNI.jpg" alt="Bijou captures the best of modern, Euro-chic design. The clean lines of its rich Polished Nickel laser cut frame feature elegant strands of crystal beads that span the entire chandelier for a dramatic effect." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=505202&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/60-9387.jpg">
                                <img src="images/portfolio/60-9387.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=247489&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/16-7118-PN.png">
                                <img src="images/portfolio/16-7118-PN.png" alt="Icy crystal prisms glisten across the Alpine collection. This striking design stacks Hollywood glamour on a rigidly geometric frame, creating an unexpected cubist masterpiece. Subtly staggered clusters of candelabra lamps cast a sparkling glow along Alpine's ethereal frame, extending a brilliant light pattern into any room. Polished Nickel preserves the fixture's winter chill, while Aged Brass accents add warming contrast." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=427634&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/2-517-GA.jpg">
                                <img src="images/portfolio/2-517-GA.jpg" alt="From the French brooch, the Broche collection lights up a room with tailored elegance. The simple wrought iron leaves on each light are hand painted in one of two metallic finishes - burnished antique gold or English bronze. There's also a two-tone sphere option that embraces one of fashion's hottest trends - mixing metals." class="img-responsive">
                            </a>    
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=521844&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/615311.jpg">
                                <img src="images/portfolio/615311.jpg" alt="This 6 light chandelier from the Abellona Collection is an alluring reflection of traditional style. The elegant sweeping arms and Sterling Gold finish are highlighted with delicate crystal beading draped beneath. All are beautiful details that unite for a breathtaking whole." class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=615311&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/60-9000-0014.jpg">
                                <img src="images/portfolio/60-9000-0014.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=731931&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/67-135-46.jpg">
                                <img src="images/portfolio/67-135-46.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=382240&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/67-176-710.jpg">
                                <img src="images/portfolio/67-176-710.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=556087&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/67-204-46.jpg">
                                <img src="images/portfolio/67-204-46.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=668500&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/67-207-48.jpg">
                                <img src="images/portfolio/67-207-48.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=668516&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/67-230-11.jpg">
                                <img src="images/portfolio/67-230-11.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=710872&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/107-B4272.jpg">
                                <img src="images/portfolio/107-B4272.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=629551&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/107-F2996.jpg">
                                <img src="images/portfolio/107-F2996.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=430219&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/107-F4106.jpg">
                                <img src="images/portfolio/107-F4106.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=561926&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/20-CA2525.jpg">
                                <img src="images/portfolio/20-CA2525.jpg" alt="" class="img-responsive">
                            </a>
                        </div>

                        <div class="clearfix"></div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/20-DV1212A.jpg">
                                <img src="images/portfolio/20-DV1212A.jpg" alt="" class="img-responsive">
                            </a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/67-218-42.jpg">
                                <img src="images/portfolio/67-218-42.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=710846&page=1">BUY NOW</a>
                        </div>

                        <div class="gallery-item col-md-4 col-sm-6">
                            <a class="lightbox" href="images/portfolio/67-128-44.jpg">
                                <img src="images/portfolio/67-128-44.jpg" alt="" class="img-responsive">
                            </a>
                            <a class="buy" target="blank" href="http://elementsathome.com/search_individual_result.asp?current=315071&page=1">BUY NOW</a>
                        </div>

                    </div>
                </div>
            </div>
            <div class="styles">
                <div class="row">
                    <div class="col-md-2"><i class="fa fa-caret-right" aria-hidden="true"></i> Hollywood Glam </div>
                    <div class="col-md-2"><i class="fa fa-caret-right" aria-hidden="true"></i> Timeless Elegance </div>
                    <div class="col-md-2"><i class="fa fa-caret-right" aria-hidden="true"></i> Cutting Edge </div>
                    <div class="col-md-2"><i class="fa fa-caret-right" aria-hidden="true"></i> Modern Classic </div>
                    <div class="col-md-2"><i class="fa fa-caret-right" aria-hidden="true"></i> Sophisticated </div>
                    <div class="col-md-2"><i class="fa fa-caret-right" aria-hidden="true"></i> Farmhouse Chic </div>
                </div>
            </div>
        </div>           
    </div>
</section>
<?php include ('inc/footer.php'); ?>