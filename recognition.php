<?php
$pageTitle = "Recognition | Elements Distictive Lighting & Home Furnishings";
$pageDescription = "Description";
$thisPage = "Recognition";
$servicePage = false;
include('inc/header.php'); ?>
  <section class="insidecontent quizinner">
    <div class="row">
      <div class="col-md-12">
        <div class="stylequiz-recognition">
          <h1>Awards &amp; Recognition</h1>
          <p>Elements Distinctive Lighting is the winner of multiple awards, as well as the host of the Met Gala Event</p>
        </div>
        <div class="container-fluid">
          <div class="content">
            <div class="row">
              <div class="col-sm-6">
                <p class="h2">2006, 2012, 2013</p>
                <p>Elements Winner of National Arts Award for Lighting Showroom of the Year</p>
                <br>
                <img src="images/awards.png" class="award-image">
              </div>
              <div class="col-sm-6 ">
                <p class="h2 ">2013</p>
                <p>TLC's Long Island Medium, Theresa Caputo, chooses Elements to do a segment for her nationally syndicated show</p>
                <img src="images/li-medium.png" class="award-image">
              </div>
            </div>
            <div class="row ">
              <p class="h2 ">The Met Gala Event</p>
              <p>Elements was awarded the job of fabricating the crystal chandeliers for the 2014 Met Gala at the Metropolitan Museum of Art, New York City. The design of the chandeliers was by world famous fashion icon, Oscar de la Renta, just prior to his passing. His beautiful pieces were the finishing touch to an absolutely stunning event, and lived up to the standard of glamour and drama one expects from such a prestigious affair.</p>
            </div>
          </div>
        </div>
        <!-- <div class="styles ">
          <div class="row ">
            <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Hollywood Glam </div>
            <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Timeless Elegance </div>
            <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Cutting Edge </div>
            <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Modern Classic </div>
            <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Sophisticated </div>
            <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Farmhouse Chic </div>
          </div>
        </div> -->
      </div>
  </section>
  <?php include ('inc/footer.php'); ?>
