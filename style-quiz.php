<?php
$pageTitle = "Style Quiz | Elements Distictive Lighting & Home Furnishings";
$pageDescription = "Description";
$thisPage = "Style Quiz";
$servicePage = false;
include('inc/header.php'); ?>




<section class="insidecontent quizinner">
 
    <div class="row">
        <div class="col-md-12">
        <div class="stylequiz">
            <h1>Define Your Style</h1>
            <p>What's your design style? Not sure? Take our quiz and find out!</p>
        </div>
         <div class="container-fluid">
            <div class="content"> 
                    <h2>Discover Your Style</h2>
                    <p>When you walk into a room that speaks to you on every level, do you know how to describe the style? We can help you with our "Define Your Style Quiz". By the end of the quiz, you will know if your style is: Timeless Elegance, Hollywood Glam, Modern Classic, Cutting Edge, Sophisticated, Farmhouse Chic. </p>
                    <div class="btn btn-primary"><a href="style-quiz-fashion-designer"><i class="fa fa-caret-right" aria-hidden="true"></i> Start Now</a></div>
             </div>
           </div>
           <div class="styles">
               <div class="row">
                   <div class="col-md-2"><i class="fa fa-caret-right" aria-hidden="true"></i> Hollywood Glam </div>
                   <div class="col-md-2"><i class="fa fa-caret-right" aria-hidden="true"></i> Timeless Elegance </div>
                   <div class="col-md-2"><i class="fa fa-caret-right" aria-hidden="true"></i> Cutting Edge </div>
                   <div class="col-md-2"><i class="fa fa-caret-right" aria-hidden="true"></i> Modern Classic </div>
                   <div class="col-md-2"><i class="fa fa-caret-right" aria-hidden="true"></i> Sophisticated </div>
                   <div class="col-md-2"><i class="fa fa-caret-right" aria-hidden="true"></i> Farmhouse Chic </div>
               </div>
           </div>
           
         </div>
</section>

<?php include ('inc/footer.php'); ?>

