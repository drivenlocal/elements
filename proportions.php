<?php
$pageTitle = "Proportion Guide | Elements Distictive Lighting & Home Furnishings";
$pageDescription = "Description";
$thisPage = "Proportion Guide";
$servicePage = false;
include('inc/header.php'); ?>
  <section class="insidecontent quizinner">
    <div class="row">
      <div class="col-md-12">
        <div class="stylequiz-proportions">
          <h1>Proportion Guide</h1>
          <p>Click On Each Category to Download PDF</p>
        </div>
        <div class="container-fluid">
          <div class="content proportions-page">
            <div class="row">
              <p class="h2"><a href="pdf/dining-room.pdf" target="_blank">Dining Room</a></p>
              <div class="col-sm-6 prop-pic">
                <img src="images/dining-prop.jpg">
              </div>
              <div class="col-sm-6">
                <p class="h3">1/2 Width of Table and Add 5 Inches (Rectangle Table)</p>
                <p>Ceiling height - 8ft (over all height of chandelier should not exceed 30-32 inches. This will allow proper chain and canopy proportion to exist - 36 inches from bottom of fixture to top of canopy)</p>
                <div class="row">
                  <div class="col-sm-6">
                    <p class="h4 dining-prop var">Variables:</p>
                  </div>
                  <div class="col-sm-6">
                    <p>A - Elogated Table</p>
                    <p>Use 2 petite Chandeliers 1/2 Wd of Table</p>
                    <hr>
                    <p>B - Round Table</p>
                    <p>Use 1/2 Wd of Table</p>
                    <hr>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <p class="h4 dining-prop var">Table Size:</div>
                  <div class="col-sm-6">
                    <p>Wd: 45"</p>
                    <p>Length:84"</p>
                    <p>Chandelier Size: 28-29"</p>
                    <hr>
                    <p>Wd: 50"</p>
                    <p>Length: 102"</p>
                    <p>Chandelier Size: 30-32"</p>
                    <hr>
                    <p>Wd: 60" Round</p>
                    <p>Chandelier Size: 30-33"</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="row category-row">
              <p class="h2"><a href="pdf/foyer.pdf" target="_blank">Foyer</a></p>
              <div class="col-sm-6 prop-pic">
                <img src="images/foyer-prop.jpg">
              </div>
              <div class="col-sm-6">
                <p class="h3">1/3 of Width of Space - 2 Story Entry Foyer</p>
                <div class="col-sm-6 foyer-prop">
                  <p class="h4 var">Space:</p>
                </div>
                <div class="col-sm-6">
                  <div class="row">
                    <div class="col-sm-6">
                      <p>A - Wd: 10ft</p>
                      <p>Ht: 18ft</p>
                      <p>Depth: 10ft</p>
                      <hr>
                    </div>
                    <div class="col-sm-6">
                      <p>Chandelier Size-
                        <br>36-40"</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <p>B - Wd: 14ft</p>
                      <p>Ht: 22ft</p>
                      <p>Depth: 12ft</p>
                      <hr>
                    </div>
                    <div class="col-sm-6">
                      <p>Chandelier Size-
                        <br>50-55"</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <p>Wd: 7ft</p>
                      <p>Ht: 14ft</p>
                      <p>Depth: 10ft</p>
                    </div>
                    <div class="col-sm-6">
                      <p>Chandelier Size-
                        <br>30-33"</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row category-row">
              <p class="h2"><a href="pdf/ceiling-fans.pdf" target="_blank">Ceiling Fans</a></p>
              <div class="col-sm-6 prop-pic">
                <img src="images/ceiling-prop.jpg">
              </div>
              <div class="col-sm-6">
                <div class="col-sm-4">
                  <p class="h4 var">Room Size:</p>
                </div>
                <div class="col-sm-4">
                  <p>8 X 8 ft</p>
                  <p>10 X 10 ft</p>
                  <p>12 X 12 ft</p>
                  <p>14 X 14 ft</p>
                  <p>20 X 20 ft</p>
                </div>
                <div class="col-sm-4">
                  <p>29-32" Fan</p>
                  <p>42" Fan</p>
                  <p>48" Fan</p>
                  <p>52" Fan</p>
                  <p>60" Fan</p>
                </div>
                <p>All fans are required to be 18 inches from the blade tip to the wall or obstruction. Most fans are made for 8 ft ceilings, but when there is an intergrated up light or the fan has an oah greater than 12", a taller ceiling is required.
                  <p>Proper Fan Down Rods Based On Ceiling Height:</p>
                  <div class="col-sm-6">
                    Ceiling
                    <p>8 ft</p>
                    <p>9 ft</p>
                    <p>10 ft</p>
                    <p>12 ft</p>
                    <p>14 ft</p>
                    <p>16 ft</p>
                    <p>18 ft</p>
                    <p>20 ft</p>
                  </div>
                  <div class="col-sm-6">
                    Down Rod
                    <p>2"</p>
                    <p>6"</p>
                    <p>12"</p>
                    <p>24"</p>
                    <p>36"</p>
                    <p>48"</p>
                    <p>60"</p>
                    <p>72"</p>
                  </div>
              </div>
            </div>
            <div class="row category-row">
              <p class="h2"><a href="pdf/kitchen-islands.pdf" target="_blank">Kitchen Island</a></p>
              <p>Proper Spacing of Pendents</p>
              <div class="col-sm-6 prop-pic">
                <img src="images/kitchen-prop.jpg">
              </div>
              <div class="col-sm-6">
                <div class="row kitchen-prop">
                  <div class="col-sm-4">
                    <p>Mini Pendent:</p>
                  </div>
                  <div class="col-sm-4">
                    <p>Wd: 6-8"</p>
                  </div>
                  <div class="col-sm-4">
                    <p>OA Ht: 30"</p>
                  </div>
                  <img src="images/mini-pendent.png">
                </div>
                <div class="row kitchen-prop">
                  <div class="col-sm-4">
                    <p>Standard&nbsp;Pendent:</p>
                  </div>
                  <div class="col-sm-4">
                    <p>Wd: 12-18"</p>
                  </div>
                  <div class="col-sm-4">
                    <p>OA Ht: 30"</p>
                  </div>
                  <img src="images/standard-pendent.png">
                </div>
                <div class="row kitchen-prop">
                  <div class="col-sm-6">
                    <p>Billard/Bar Fixture:</p>
                  </div>
                  <div class="col-sm-6">
                    <p>Wd: 1/2 Length of Island</p>
                  </div>
                  <img src="images/billiard.png">
                </div>
                <p>Based on 7' Island</p>
              </div>
            </div>
            <div class="row category-row">
              <p class="h2"><a href="pdf/recessed-lighting.pdf" target="_blank">Recessed Lighting</a></p>
              <p class="h4 var">Sizing To Room</p>
              <div class="col-sm-6 prop-pic">
                <img src="images/recessed-prop.jpg">
              </div>
              <div class="col-sm-6 kitchen-prop">
                <div class="col-sm-6">
                  <p>6" Down Ltf R40/Par38</p>
                  <p>5" Down Ltg R30/Par30</p>
                  <p>4" Down Ltg R20/Par20</p>
                  <p>4" Lv Down Ltg Mr16</p>
                </div>
                <div class="col-sm-6">
                  <p>General</p>
                  <p>Hall/Bed</p>
                  <p>Task Niche</p>
                  <p>Accent</p>
                </div>
              </div>
            </div>
            <div class="row category-row">
              <p class="h2"><a href="pdf/cove-lighting.pdf" target="_blank">Cove Lighting</a></p>
              <p>Low Voltage Cove Runs</p>
              <div class="col-sm-6 prop-pic">
                <img src="images/cove-prop.jpg">
              </div>
              <div class="col-sm-6">
                <p>3" Spacing 5 Watts</p>
                <p>4" Spacing 5 Watts</p>
                <p>5" Spacing 3 Watts</p>
                <div class="col-sm-6">
                  <p class="h4 var">Variables:</p>
                </div>
                <div class="col-sm-6">
                  <p>Straight Run</p>
                  <p>Radius Run</p>
                  <p>Light Distribution</p>
                  <p>Color Rendering</p>
                </div>
              </div>
            </div>
            <div class="row category-row">
              <p class="h2"><a href="pdf/outdoor-lighting.pdf" target="_blank">Outdoor Lighting</a></p>
              <div class="col-sm-6 prop-pic">
                <img src="images/outdoor-prop.jpg">
              </div>
              <div class="col-sm-6">
                <p class="h3">Outdoor Product Proportions</p>
                <div class="col-sm-4">
                  <p>Front&nbsp;Door&nbsp;Single</p>
                  <p>Front&nbsp;Door&nbsp;Double</p>
                  <p>Side/Back/Slider</p>
                  <p>Pier&nbsp;24"&nbsp;Square</p>
                  <p>Pole</p>
                  <p>Hanging</p>
                </div>
                <div class="col-sm-4">
                  <p>Wd: 12"</p>
                  <p>Wd: 14-16"</p>
                  <p>Wd: 9"</p>
                  <p>Wd: 13"</p>
                  <p>Wd: 10"</p>
                  <p>Wd: 14"</p>
                </div>
                <div class="col-sm-4 right-col">
                  <p>Ht: 18"</p>
                  <p>Ht: 21-25"</p>
                  <p>Ht: 14"</p>
                  <p>Ht: 25"</p>
                  <p>Ht: 22"</p>
                  <p>Ht: 22"</p>
                </div>
                <p class="h4 var">Variables:</p>
                <p>A- Units Facing Up or Down</p>
                <p>B- Pier Mounted Units Should be 1/2 the Width of the Top of the Pier</p>
                <p>C- Landscape Lighting: - Group in Three
                  <br> - Path, Driveway, Moon Lighting, Pool, etc</p>
              </div>
            </div>
          </div>
          <!-- End Content Div -->
        </div>
        <!-- <div class="styles ">
  <div class="row ">
    <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Hollywood Glam </div>
    <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Timeless Elegance </div>
    <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Cutting Edge </div>
    <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Modern Classic </div>
    <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Sophisticated </div>
    <div class="col-md-2 "><i class="fa fa-caret-right " aria-hidden="true "></i> Farmhouse Chic </div>
  </div>
</div>
 -->
      </div>
  </section>
  <?php include ('inc/footer.php'); ?>
