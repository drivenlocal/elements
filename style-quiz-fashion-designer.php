<?php
$pageTitle = "Style Quiz | Elements Distictive Lighting & Home Furnishings";
$pageDescription = "Description";
$thisPage = "Style Quiz";
$servicePage = false;
include('inc/header.php'); ?>
  <form>
    <!-- Counters -->
    <input id="count_hg" type="hidden" value="0"> 
    <input id="count_te" type="hidden" value="0">
    <input id="count_ce" type="hidden" value="0">
    <input id="count_mc" type="hidden" value="0">
    <input id="count_fc" type="hidden" value="0"> 
    <input id="count_s" type="hidden" value="0"> 
    <input id="hdnImages" type="hidden">
  </form>
  <section class= "insidecontent quizinner">
    <div class="row">
      <div class="col-md-12">
        <div class="container-fluid">
          <div class="styleinner">
            <h1>Elements Style Quiz</h1>
          </div>
          <div class="content">
            <!-- Question 1 -->
            <div id="divQuestion1" style="display: block;">
              <h2>Which fashion designer’s clothes would you love hanging in your closet?</h2>
              <div class="row row-centered">
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['te', 'hg', 's'], 2, 'd1.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/d1.jpg">
                  </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['te', 'hg', 's'], 2, 'd3.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/d3.jpg">
                  </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['mc', 'fc'], 2, 'd4.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/d4.jpg">
                  </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['mc'], 2, 'd5.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/d5.jpg">
                  </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['hg', 'ce'], 2, 'd7.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/d7.jpg">
                  </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['fc'], 2, 'd8.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/d8.jpg">
                  </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['ce', 'hg'], 2, 'd11.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/d11.jpg">
                  </a>
                </div>
              </div>
            </div>
            <!-- Question 2 -->
            <div id="divQuestion2" style="display: none;">
              <h2>What dining room invites you to sit down for dinner?</h2>
              <div class="row row-centered">
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['ce', 'mg'], 3, 'dr_bwstripes.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/dr_bwstripes.jpg">
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['ce', 'hg', 's'], 3, 'dr_bw.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/dr_bw.jpg">
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['te', 's'], 3, 'dr_crystal.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/dr_crystal.jpg">
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['fc'], 3, 'dr_farmhouse.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/dr_farmhouse.jpg">
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['mc', 'ce'], 3, 'dr_globes.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/dr_globes.jpg">
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['mc', 'fc'], 3, 'dr_orange.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/dr_orange.jpg">
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['mc', 's'], 3, 'dr_redshades.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/dr_redshades.jpg">
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['cd', 'hg'], 3, 'dr_redvelvet.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/dr_redvelvet.jpg">
                  </a>
                </div>
              </div>
            </div>
            <!-- Question 3 -->
            <div id="divQuestion3" style="display: none;">
              <h2>Which backyard would you love to spend time in?</h2>
              <div class="row row-centered">
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['fc'], 4, 'by_adirondak.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/by_adirondak.jpg">
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['mc', 's'], 4, 'by_bluepillow.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/by_bluepillow.jpg">
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['ce', 'hg'], 4, 'by_mirrors.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/by_mirrors.jpg">
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['ce'], 4, 'by_redorange.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/by_redorange.jpg">
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['fc'], 4, 'by_rockpath.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/by_rockpath.jpg">
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['te', 'mc'], 4, 'by_whitehouse.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/by_whitehouse.jpg">
                  </a>
                </div>
                <div class="col-sm-4 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['mc', 'te'], 4, 'by_yellow.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/by_yellow.jpg">
                  </a>
                </div>
              </div>
            </div>
            <!-- Question 4 -->
            <div id="divQuestion4" style="display: none;">
              <h2>What dessert would you splurge on your diet?</h2>
              <div class="row row-centered">
                <div class="col-sm-6 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['fc', 'te'], 5, 'cookies.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/cookies.jpg">
                  </a>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['te', 'mc', 's'], 5, 'cake.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/cake.jpg">
                  </a>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['s', 'hg', 'ce'], 5, 'tart.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/tart.jpg">
                  </a>
                </div>
                <div class="col-sm-6 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['ce'], 5, 'everything.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/everything.jpg">
                  </a>
                </div>
              </div>
            </div>
            <!-- Question 5 -->
            <div id="divQuestion5" style="display: none;">
              <h2>What  vehicle would you most want to drive?</h2>
              <div class="row row-centered">
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['s', 'te'], 6, 'benz.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/benz.jpg">
                  </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['te', 's'], 6, 'bmw.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/bmw.jpg">
                  </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['mc'], 6, 'lexus.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/lexus.jpg">
                  </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['te'], 6, 'cadillac.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/cadillac.jpg">
                  </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['ce', 'hg'], 6, 'lambo.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/lambo.jpg">
                  </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['hg', 'te'], 6, 'bently.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/bently.jpg">
                  </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['mc', 's', 'fc'], 6, 'range.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/range.jpg">
                  </a>
                </div>
                <div class="col-sm-3 col-xs-6">
                  <a style="cursor: pointer;" onclick="addAnswer(['fc', 'ce'], 6, 'truck.jpg');">
                    <img class="element img-responsive" src="images/stylequiz/truck.jpg">
                  </a>
                </div>
              </div>
            </div>

            <!-- Results -->
            <div id="results" style="display: none; text-align: center;">
              <h1><label id="res_title"></label></h1>
              <p>
                Now, wasn't that fun?! You are ready to search for the lighting that matches your taste and style. Happy shopping!
              </p>
              <div class="row row-centered">
                <div class="col-sm-4 col-xs-6">
                  <img class="element img-responsive" id="imgRes2" src="">
                </div>
                <div class="col-sm-4 col-xs-6">
                  <img class="element img-responsive" id="imgRes3" src="">
                </div>
                <div class="col-sm-4 col-xs-6">
                  <img class="element img-responsive" id="imgRes4" src="">
                </div>
                <div class="col-sm-4 col-xs-6">
                  <img class="element img-responsive" id="imgRes5" src="">
                </div>
                <div class="col-sm-4 col-xs-6">
                  <img class="element img-responsive" id="imgRes1" src="">
                </div>
              </div>
              <hr>
              <!-- <div id="res_hg" style="display: none; margin-top: 16px;">
                <div style="text-align: center; margin: 32px 0;">
                </div>
              </div>
              <div id="res_te" style="display: none; margin-top: 16px;">
                <div style="text-align: center; margin: 32px 0;">
                </div>
              </div>
              <div id="res_ce" style="display: none; margin-top: 16px;">
                <div style="text-align: center; margin: 32px 0;">
                </div>
              </div>
              <div id="res_mc" style="display: none; margin-top: 16px;">
                <div style="text-align: center; margin: 32px 0;">
                </div>
              </div>
              <div id="res_fc" style="display: none; margin-top: 16px;">
                <div style="text-align: center; margin: 32px 0;">
                </div>
              </div>
              <div id="res_s" style="display: none; margin-top: 16px;">
                <div style="text-align: center; margin: 32px 0;">
                </div>
              </div> -->
            </div>
          </div>
        </div>
        <div class="styles">
          <div class="row">
            <div class="col-md-2 col-xs-6">
              <i aria-hidden="true" class="fa fa-caret-right"></i> Hollywood Glam
            </div>
            <div class="col-md-2 col-xs-6">
              <i aria-hidden="true" class="fa fa-caret-right"></i> Timeless Elegance
            </div>
            <div class="col-md-2 col-xs-6">
              <i aria-hidden="true" class="fa fa-caret-right"></i> Cutting Edge
            </div>
            <div class="col-md-2 col-xs-6">
              <i aria-hidden="true" class="fa fa-caret-right"></i> Modern Classic
            </div>
            <div class="col-md-2 col-xs-6">
              <i aria-hidden="true" class="fa fa-caret-right"></i> Sophisticated
            </div>
            <div class="col-md-2 col-xs-6">
              <i aria-hidden="true" class="fa fa-caret-right"></i> Farmhouse Chic
            </div>
          </div>
        </div>
      </div>
    </div>
</section>

<?php include ('inc/footer.php'); ?>

