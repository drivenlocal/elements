function addAnswer(styles, nextQuestion, imgName) {
  var count;
  if ($.inArray('hg', styles) > -1) {
    count = parseInt($('#count_hg').val());
    $('#count_hg').val(count + 1);
  } 
  if ($.inArray('te', styles) > -1) {
    count = parseInt($('#count_te').val());
    $('#count_te').val(count + 1);
  } 
  if ($.inArray('ce', styles) > -1) {
    count = parseInt($('#count_ce').val());
    $('#count_ce').val(count + 1);
  } 
  if ($.inArray('mc', styles) > -1) {
    count = parseInt($('#count_mc').val());
    $('#count_mc').val(count + 1);
  } 
  if ($.inArray('fc', styles) > -1) {
    count = parseInt($('#count_fc').val());
    $('#count_fc').val(count + 1);
  } 
  if ($.inArray('s', styles) > -1) {
    count = parseInt($('#count_s').val());
    $('#count_s').val(count + 1);
  }

  //add to img list
  var hdnImages = $('#hdnImages');
  var curImages = hdnImages.val();
  hdnImages.val(curImages + imgName + ",");

  //if 6, quiz is over, do final tasks
  if (nextQuestion == 6) {
    var style = "You are HOLLYWOOD GLAM!";
    var res = $('#res_hg');

    var hg = parseInt($('#count_hg').val());
    var te = parseInt($('#count_te').val());
    var ce = parseInt($('#count_ce').val());
    var mc = parseInt($('#count_mc').val());
    var fc = parseInt($('#count_fc').val());
    var s  = parseInt($('#count_s').val());

    var curMax = hg;

    if (te > curMax) {
      style = "You are TIMELESS ELEGANCE!";
      curMax = te;
      res = $('#res_te');
    }
    if (ce > curMax) {
      style = "You are CUTTING EDGE!";
      curMax = ce;
      res = $('#res_ce');
    }
    if (mc > curMax) {
      style = "You are MODERN CLASSIC!";
      curMax = mc;
      res = $('#res_mc');
    }
    if (fc > curMax) {
      style = "You are FARMHOUSE CHIC!";
      curMax = fc;
      res = $('#res_fc');
    }
    if (s > curMax) {
      style = "You are SOPHISTICATED!";
      curMax = s;
      res = $('#res_s');
    }

    //Result images
    var imgs = $('#hdnImages').val();
    if (imgs.substr(-1) == ",")
      imgs = imgs.slice(0, -1);
    var imgArr = imgs.split(',');
    $('#imgRes1').attr('src', 'images/stylequiz/' + imgArr[0]);
    $('#imgRes2').attr('src', 'images/stylequiz/' + imgArr[1]);
    $('#imgRes3').attr('src', 'images/stylequiz/' + imgArr[2]);
    $('#imgRes4').attr('src', 'images/stylequiz/' + imgArr[3]);
    $('#imgRes5').attr('src', 'images/stylequiz/' + imgArr[4]);

    //Results
    $('#res_title').text(style);
    $('#divQuestion' + (nextQuestion - 1)).fadeOut('fast', function() {
      $('#results').fadeIn('fast');
      res.fadeIn('fast');
    });

  } else {
    $('#divQuestion' + (nextQuestion - 1)).fadeOut('fast', function() {
      $('#divQuestion' + nextQuestion).fadeIn('fast');
    });
  }

}
