jQuery(document).ready(function($) {

	//gallery
	$('.gallery').magnificPopup({
	type: 'image',
	delegate: '.lightbox',
	gallery:{
	  enabled:true
	},
	image: {
	  titleSrc: function(item) {
	    return item.el.children().attr('alt');
	  }
	}
	});

});   